const books = [
  {
    title: "Financial Freedom A Proven Path to All the Money You Will Ever Need",
    author: "Grant Sabatier Vicki Robin",
    image: "financial.png",
    downloadLink: "https://www.audible.fr/pd/Financial-Freedom-Livre-Audio/1984846132?overrideBaseCountry=true&bp_o=true&asin=1984846132&source_code=ADWPP30DTRIAL45304132390C8&ipRedirectOverride=true&gclid=CjwKCAiA9ourBhAVEiwA3L5RFhZJ6QUExH_rcJCefo1oHN7-_MdcGhoKOl4PssxAoL2ZPa26-PNaMBoCXCYQAvD_BwE"
  },
  
  // Add more books here
];

function displayBooks() {
  const bookList = document.getElementById('book-list');

  books.forEach(book => {
    const bookDiv = document.createElement('div');
    bookDiv.classList.add('book');

    const image = document.createElement('img');
    image.src = book.image;
    image.alt = book.title + " Cover";
    bookDiv.appendChild(image);

    const title = document.createElement('h2');
    title.textContent = book.title;
    bookDiv.appendChild(title);

    const author = document.createElement('p');
    author.textContent = "By " + book.author;
    bookDiv.appendChild(author);

    const downloadBtn = document.createElement('button');
    downloadBtn.textContent = "Get the book now";
    downloadBtn.addEventListener('click', () => {
      window.location.href = book.downloadLink;
    });
    bookDiv.appendChild(downloadBtn);

    bookList.appendChild(bookDiv);
  });
}

window.onload = displayBooks;
